import styles from '@/styles';
import {
  BillingComponent,
  BusinessComponent,
  CardDealComponent,
  ClientsComponent,
  CTAComponent,
  FooterComponent,
  HeroComponent,
  NavbarComponent,
  TestimonialsComponent,
  StatsComponent
} from '@/components';

const App = () => {
  return (
    <div className="bg-primary w-full overflow-hidden">
      <div className={`${styles.paddingX} ${styles.flexCenter}`}>
        <div className={`${styles.boxWidth}`}>
          <NavbarComponent />
        </div>
      </div>

      <div className={`bg-primary ${styles.flexStart}`}>
        <div className={`${styles.boxWidth}`}>
          <HeroComponent />
        </div>
      </div>
      
      <div className={`bg-primary ${styles.paddingX} ${styles.flexCenter}`}>
        <div className={`${styles.boxWidth}`}>
          <StatsComponent />
          <BusinessComponent />
          <BillingComponent />
          <CardDealComponent />
          <TestimonialsComponent />
          <ClientsComponent />
          <CTAComponent />
          <FooterComponent />
        </div>
      </div>
    </div>
  );
};

export default App;

